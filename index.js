/*
*  Primary File for the API
*
* */


// Dependencies
const server = require('./lib/server');
const c = require('./lib/consoleLog');

// Declaring app
var app = {};



app.init = function() {
    // start server:
    server.init();


};

app.init();


module.exports = app;



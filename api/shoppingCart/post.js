/*
*
*
*
* */



// Dependencies
const _data = require('../../lib/data');
const orderPrice = require('../../lib/orderPrice');


module.exports.handler = (data, callback) => {
    let orders = data.payload.order;

    if(!orders || typeof orders !== 'object' && !Array.isArray(orders) && orders.length == 0){
        callback(400, {"Error" : "Payload is invalid."})
    } else {
        orderPrice.getOrderCost(orders, function (err, orderReceipt) {
            if(err){
                callback(500, orderReceipt);
            } else {
                // create order file, return results
                orderReceipt[0].email = data.queryStringObject.email ? data.queryStringObject.email : false;
                if(!orderReceipt[0].email){

                } else {
                    // make sure an order does not exist already for that user:
                    _data.list('cart', function (err, cartData) {
                        if (!err) {
                            if (cartData.length > 0 && cartData.indexOf(orderReceipt[0].email) > -1) {
                                callback(400, {"Error" : "an order already exist for this user. Either confirm the order or delete this order and create a new one."})
                            } else {
                                _data.create('cart', orderReceipt[0].email, orderReceipt, function (err) {
                                    if(err){
                                        callback(500, {"Error"  : "Internal error, could not create orderNumber and add it to the cart."})
                                    } else {
                                        callback(200, orderReceipt);
                                    }
                                });
                            }
                        } else {
                            callback(500, {"Error" : "Internal error"})
                        }
                    });
                }
            }
        });
    }
};




// NOTES: ------> request coming in
// const request = {
//     "pizza": [
//     {
//         "order-type"          : "pizza",
//         "order-amount"        : 2,
//         "pizza-size"    	  : "small",
//         "crust-type"    	  : "pan",
//         "cut"       		  : "normal",
//         "bake"      		  : "well-done",
//         "sauce"         	  : "original",
//         "sauce-amount"        : "extra",
//         "vegan-cheese-amount" : "normal",
//         "vegan-protein"       : ["tempeh", "jackfruit"],
//         "toppings"      	  : ["pineapple", "black ovlives", "jalapenos"]
//     },
//     {
//         "order-type"          : "side",
//         "order-amount"        : 1,
//         "side-type"           : "garlic-knots",
//         "pieces"              : "8"
//
//     },
//     {
//         "order-type"          : "dessert",
//         "order-amount"        : 1,
//         "dessert"             : "brownie",
//         "pieces"              : "8"
//
//     },
//     {
//         "order-type"          : "drink",
//         "order-amount"        : 1,
//         "drink-type"          : "canada-dry",
//         "drink-size"          : "2-liter"
//
//     },
//     {
//         "order-type"          : "extra",
//         "order-amount"        : 3,
//         "extra-type"          : "garlic-sauce"
//
//     }
//  ]
// }



// working endpoint payload:
// {
//     "order": [
//     {
//         "order-type"          : "pizza",
//         "order-amount"        : 3,
//         "pizza-size"    	  : "small",
//         "crust-type"    	  : "pan",
//         "cut"       		  : "normal",
//         "bake"      		  : "well-done",
//         "sauce"         	  : "original",
//         "sauce-amount"        : "extra",
//         "cheese-amount"       : "normal",
//         "vegan-protein"       : ["tempeh", "jackfruit"],
//         "toppings"      	  : ["pineapple", "black olives", "jalapenos"]
//     }
// ]
// }






























/*
*
*
*
*
* */

// Dependencies
const _data = require('../../lib/data');
const val = require('../validations/orderPost');
const helpers = require('../../lib/helpers');


module.exports.handler = (data, callback) => {


    var email = data.queryStringObject.email && typeof data.queryStringObject.email == 'string' ? data.queryStringObject.email : false;

    if(!email){
        callback(400,{"Error" : "Invalid email queryString."})
    } else {
        // check if an order already exist in the cart, if not then a purchase cannot be made.
        _data.read('cart', email, function (err, cartData) {
            if(err) {
                callback(400, {"Error" : "Cart is empty."})
            } else {
                // validate payload:
                val.check(data.payload, function (err, errorMessage) {
                    if(err){
                        callback(400, errorMessage)
                    } else {
                        // validation passed, now get order price and charge the card, then remove from cart collection and put in ordered collection
                        // send email and successful response: 
                        
                        helpers.payment({amount: cartData[0].totalCost, description: 'Pizza App Test Payment'}, function (result) {


                            helpers.sendEmail(JSON.stringify(cartData, null, 3), '', 'Pizza Receipt', email, function (statusCode, results) {
                                if(statusCode == 200){
                                    // delete cart and move to ordered:
                                    _data.delete('cart', email, function (err) {

                                        if (err) {
                                            callback(400, {"Error" : "Could not remove from cart. Pizza is now being prepared and an email has been sent."})
                                        } else {
                                            _data.create('ordered', email + ' ' + cartData[0].orderNumber, cartData, function (err) {
                                                if (err){
                                                    callback(400, {"Error" : "Could not move cart data into ordered collection. Pizza is now being prepared and an email has been sent."})
                                                } else {
                                                    cartData[0].orderComplete = true;
                                                    cartData[0].emailSent = true;
                                                    cartData[0].message = 'Thank you for your purchase. Your order is now being prepared.';
                                                    callback(200, cartData);
                                                }
                                            });
                                        }
                                    });
                                } else {
                                    callback(400, {"Error" : "email was not sent. Order is now in progress"})
                                }
                            })
                        })
                    }
                })
            }
        });
    }
};




























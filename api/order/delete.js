/*
*
*  Deleting a  order before purchase
*
*
* */

// Dependencies
const _data = require('../../lib/data');


module.exports.handler = (data, callback) => {

    if(!data.queryStringObject.email) {
        callback(400, {"Error" : "Missing email query string."})
    } else {

        _data.read('cart', data.queryStringObject.email, function (err, cartData) {
            if(err){
                callback(400, {"Error" : "Cart is empty."});
            } else {
                // NOTES: refactor if there are references to other collections down the road:
                _data.delete('cart', data.queryStringObject.email, function (err) {
                    if(err){
                        callback(500, {"Error" : "Internal error, could not delete user."});
                    } else {
                        callback(200, {"Message" : "Your cart is now empty."});
                    }
                });
            }
        });
    }
};






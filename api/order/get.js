/*
*
*
*
* */


// Dependencies
const _data = require('../../lib/data');



module.exports.handler = (data, callback) => {
    var email = data.queryStringObject.email && typeof data.queryStringObject.email == 'string' ? data.queryStringObject.email : false;

    if(!email){
        callback(400,{"Error" : "Invalid email queryString."})
    } else {
        // Get cart info info:
        _data.read('cart', email, function (err, cartData) {
            if(err){
                callback(400, {"Error" : "Cart is empty."})
            } else {
                callback(200, cartData)
            }
        });
    }
};

































/*
*
*
*
*
* */



// Dependencies
const _data = require('../../lib/data');

module.exports.handler = (data, callback) => {

    var email = data.queryStringObject.email && typeof data.queryStringObject.email == 'string' ? data.queryStringObject.email : false;
    var token = data.headers.token;

    if(!email) {
        callback(400, {"Error" : "Missing email query string."});
    } else {
        // find token
        if(!token){
            callback(400, {"Error" : "Token not provided."});
        }
        _data.read('sessions', token, function (err, tokenData) {
            if(err) {
                callback(400, {"Error" : "Token not found."});
            } else {
                if(tokenData.email !== email){
                    callback(400, {"Error" : "Unauthorize to delete this token"});
                } else {
                    _data.delete('sessions', token, function (err) {
                        if(err) {
                            callback(500, {"Error" : "Internal error, could not delete token."});
                        } else {
                            callback(200, {"Message" : "Token was delete"});
                        }
                    });
                }
            }
        });
    }
};























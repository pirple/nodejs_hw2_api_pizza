

// Dependencies
const _data = require('../../lib/data');
const helpers = require('../../lib/helpers');
const val = require('../validations/authPost');



// Get user data from email, check if hashedPassword matches, create token
module.exports.handler = (data, callback) => {
    var userInput = data.payload;


    val.check(userInput, function (err, results) {
        if(err) {
            callback(400, results);
        } else {
            // check users collection for matching account then see if hashedPassword match
            _data.read('users', userInput.email, function (err, userData) {
                if(err){
                    callback(400, {"Error" : "Incorrect username/password or user does not exist."})
                } else {
                    var inputHashedPassword = helpers.hash(userInput.password);

                    if (inputHashedPassword !== userData.hashedPassword){
                        callback(400, {"Error" : "Incorrect username/password or user does not exist."})
                    } else {
                        // Create token - login successful

                        var token = helpers.createRandomString(26);
                        var tokenDetails = {
                            "token"      : token,
                            "email"      : userData.email,
                            "expires"    : Date.now() + 1000 * 60 * 30,
                            "hits"       : 1,
                            "created-on" : new Date().toISOString()
                        };
                        tokenDetails.expires = new Date(tokenDetails.expires).toISOString();
                        _data.create('sessions', token, tokenDetails, function (err) {
                            if(err) {
                                callback(500, {"Error" : "Internal error, could not create token."})
                            } else {
                                callback(200, tokenDetails)
                            }
                        });
                    }
                }
            });
        }
    });

};























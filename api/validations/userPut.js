/*
*
* */


// dependencies
const helpers = require('../../lib/helpers');

var lib = {};


lib.check = (data, callback) => {
    // verify payload, make sure at least one field is passed through to update


    if(!data.queryStringObject.email){
        callback(400, {"Error" : "Missing query string, email."})
    } else {


        let firstName = data.payload.firstName && typeof data.payload.firstName == 'string' && data.payload.firstName.trim().length > 0 ? data.payload.firstName.trim() : false;
        let lastName = data.payload.lastName && typeof data.payload.lastName == 'string' && data.payload.lastName.trim().length > 0 ? data.payload.lastName.trim() : false;
        let email = data.email && typeof data.email == 'string' && data.email.trim().length > 6 ? data.email.trim() : false;
        let phoneNumber = data.payload.phoneNumber && typeof data.payload.phoneNumber == 'string' && data.payload.phoneNumber.trim().length == 10 ? data.payload.phoneNumber.trim() : false;
        let zip = data.zip && typeof data.zip == 'string' && data.zip.trim().length >= 5 ? data.zip.trim() : false;
        let address = data.payload.address && typeof data.payload.address == 'string' && data.payload.address.trim().length > 2 ? data.payload.address.trim() : false;
        let city = data.city && typeof data.city == 'string' && data.city.trim().length > 2 ? data.city.trim() : false;
        let password = data.payload.password && typeof data.payload.password == 'string' && data.payload.password.trim().length >= 7 ? data.payload.password.trim() : false;
        let confirmPassword = false;
        if (password === data.payload.confirmPassword) {
            confirmPassword = true;
        }
        let hashedPassword = false;
        if(confirmPassword){
            hashedPassword = helpers.hash(password);
        }

        if(!firstName && !lastName && !email && !phoneNumber && !zip && !address && !city && (!password && !confirmPassword) ) {
            // no fields where updated: call back 400
            callback(true, {"Error": "No valid fields were update. Please update your payload with the correct payload."});
        } else {
            let results = {};
            results.payload = {
                firstName   : firstName,
                lastName    : lastName,
                email       : email,
                phoneNumber : phoneNumber,
                zip         : zip,
                address     : address,
                city        : city,
                hashedPassword : hashedPassword
            };
            callback(false, results)
        }
    }
};

















module.exports = lib;
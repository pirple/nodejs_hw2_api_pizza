






var lib = {};


lib.check = (payload, callback) => {

    var paymentConfirmation = payload.paymentConfirmation && typeof payload.paymentConfirmation == 'boolean' ? payload.paymentConfirmation : false;
    var cardNumber = payload.cardNumber && typeof payload.cardNumber == 'string' && payload.cardNumber.length > 15 ? payload.cardNumber : false;
    var expMonth = payload.expMonth && typeof payload.expMonth == 'string' & payload.expMonth.length == 2 && payload.expMonth > 0 && payload.expMonth < 13 ? payload.expMonth : false;
    var expYear = payload.expYear && typeof payload.expYear == 'string' & payload.expYear.length == 4 && payload.expYear > 2018  ? payload.expYear : false;
    var cardHolderName = payload.cardHolderName && typeof payload.cardHolderName == 'string' && payload.cardHolderName.length >= 3 ? payload.cardHolderName : false;

    if(!paymentConfirmation) {
        callback(true, {"Error" : "Invalid paymentConfirmation."})
    }
    else if(!cardNumber){
        callback(true, {"Error" : "Invalid cardNumber."})
    }
    else if (!expMonth) {
        callback(true, {"Error" : "Invalid expMonth."})
    }
    else if (!expYear) {
        callback(true, {"Error" : "Invalid expYear."})
    }
    else if (!cardHolderName) {
        callback(true, {"Error" : "Invalid cardHolderName."})
    } else {

        let date = new Date();
        let month = date.getMonth() + 1;
        let year = date.getFullYear();

        console.log(`4422 month: ${parseInt(month)}`);
        console.log(`4422 expMonnth: ${parseInt(expMonth)}`);
        if(month >parseInt(expMonth) && year == parseInt(expYear)) {
            callback(true, {"Error" : "Invalid expMonth, card is expired."});
        }
        else if (year > expYear) {
            callback(true, {"Error" : "Invalid expYear, card is expired."})
        } else {
            callback(false)
        }
    }
};



module.exports = lib;



























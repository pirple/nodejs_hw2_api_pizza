/*
*
*  validation check for auth token
*
* */




module.exports.check = (data, callback) => {

    var email = data.email && typeof data.email == 'string' && data.email.trim().length > 6 ? data.email.trim() : false;
    var password = data.password && typeof data.password == 'string' && data.password.trim().length >= 7 ? data.password.trim() : false;

    if (!email) {
        callback(true, {"Error" : "Invalid email."})
    }
    else if (!password) {
        callback(true, {"Error" : "Invalid password."})
    } else {
        callback(false)
    }
};

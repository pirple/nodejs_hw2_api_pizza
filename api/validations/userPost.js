/*
*  validation for user post:
*  Checks if all fields are sent and correctly filled out
*
* */






// container
var val = {};

val.check = (data, callback) => {

    var firstName = data.firstName && typeof data.firstName == 'string' && data.firstName.trim().length > 0 ? data.firstName.trim() : false;
    var lastName = data.lastName && typeof data.lastName == 'string' && data.lastName.trim().length > 0 ? data.lastName.trim() : false;
    var email = data.email && typeof data.email == 'string' && data.email.trim().length > 6 ? data.email.trim() : false;
    var phoneNumber = data.phoneNumber && typeof data.phoneNumber == 'string' && data.phoneNumber.trim().length == 10 ? data.phoneNumber.trim() : false;
    var zip = data.zip && typeof data.zip == 'string' && data.zip.trim().length >= 5 ? data.zip.trim() : false;
    var address = data.address && typeof data.address == 'string' && data.address.trim().length > 2 ? data.address.trim() : false;
    var city = data.city && typeof data.city == 'string' && data.city.trim().length > 2 ? data.city.trim() : false;
    var tos = data.tos && typeof data.tos == 'boolean' ? data.tos : false;

    var password = data.password && typeof data.password == 'string' && data.password.trim().length >= 7 ? data.password.trim() : false;
    var confirmPassword = true;
    if (password !== data.confirmPassword) {
        confirmPassword = false;
    }



    // Individually checking to give a more detail error message back:
    if (!firstName) {
        callback(true, {"Error" : "Invalid firstName."});
    }
    else if (!lastName) {
        callback(true, {"Error" : "Invalid lastName."});
    }
    else if (!email) {
        callback(true, {"Error" : "Invalid email."});
    }
    else if (!phoneNumber) {
        callback(true, {"Error" : "Invalid phoneNumber."});
    }
    else if (!zip) {
        callback(true, {"Error" : "Invalid zip."});
    }
    else if (!address) {
        callback(true, {"Error" : "Invalid address."});
    }
    else if (!city) {
        callback(true, {"Error" : "Invalid city."});
    }
    else if (!tos) {
        callback(true, {"Error" : "Invalid tos."});
    }
    else if (!password) {
        callback(true, {"Error" : "Invalid password. Must be greater or equal to 7 characters."});
    }
    else if (!confirmPassword) {
        callback(true, {"Error" : "Invalid passwords do not match."});
    } else{
        callback(false)
    }
};


















module.exports = val;
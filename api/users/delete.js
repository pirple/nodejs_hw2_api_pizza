/*
*
*  Deleting a  user
*
*
* */

// Dependencies
const _data = require('../../lib/data');


module.exports.handler = (data, callback) => {

    if(!data.queryStringObject.email) {
        callback(400, {"Error" : "Missing email query string."})
    } else {

        _data.read('users', data.queryStringObject.email, function (err, userData) {
            if(err){
                callback(400, {"Error" : "User does not exist."});
            } else {
                // NOTES: refactor if there are references to other collections down the road:
                _data.delete('users', data.queryStringObject.email, function (err) {
                   if(err){
                       callback(500, {"Error" : "Internal error, could not delete user."});
                   } else {
                       callback(200, {"Message" : "All user data was deleted."});
                   }
                });
            }
        });
    }
};





































/*
*
*
*
*
* */

// Dependencies
const _data = require('../../lib/data');
const val = require('../validations/userPut');


module.exports.handler = (data, callback) => {

    // validate paylad
    val.check(data, function (err, results) {
             if(err) {
                 callback(400, results)
             }   else {
                 var payload = {
                     firstName   : results.payload.firstName,
                     lastName    : results.payload.lastName,
                     email       : results.payload.email,
                     phoneNumber : results.payload.phoneNumber,
                     zip         : results.payload.zip,
                     address     : results.payload.address,
                     city        : results.payload.city,
                     tos         : results.payload.tos,
                     hashedPassword    : results.payload.hashedPassword,
                 };

                 //get user data then update the changes:
                 _data.read('users',data.queryStringObject.email, function (err, userData) {
                     if(err) {
                         callback(400, err);
                     } else {
                         payload.firstName      = payload.firstName ? payload.firstName : userData.firstName;
                         payload.lastName       = payload.lastName ? payload.lastName : userData.lastName;
                         payload.email          = payload.email ? payload.email : userData.email;
                         payload.phoneNumber    = payload.phoneNumber ? payload.phoneNumber : userData.phoneNumber;
                         payload.zip            = payload.zip ? payload.zip : userData.zip;
                         payload.address        = payload.address ? payload.address : userData.address;
                         payload.city           = payload.city ? payload.city : userData.city;
                         payload.tos            = payload.tos ? payload.tos : userData.tos;
                         payload.hashedPassword = payload.hashedPassword ? payload.hashedPassword : userData.hashedPassword;

                         _data.update('users', data.queryStringObject.email, payload, function (err) {
                             if(err){
                                 callback(400, {"Error": "Internal error, could not update user."});
                             } else {
                                 delete payload.hashedPassword;
                                 callback(200,payload)
                             }
                         });
                     }
                 });
             }
    });
};
























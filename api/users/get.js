/*
*
*  Get user data
*
* */

// Dependencies
const _data = require('../../lib/data');


module.exports.handler = (data, callback) => {
    var email = data.queryStringObject.email && typeof data.queryStringObject.email == 'string' ? data.queryStringObject.email : false;

    if(!email){
        callback(400,{"Error" : "Invalid email queryString."})
    } else {
        // Get user info:
        _data.read('users', email, function (err, userData) {
            if(err){
                callback(400, {"Error" : "User not found."})
            } else {
                callback(200, userData)
            }
        });
    }
};























/*
*
*
*  Creating a user:
*  TODO: Define later ---->
*
* */


// Dependencies
const val = require('../validations/userPost');
const _data = require('../../lib/data');
const helpers = require('../../lib/helpers');

module.exports.handler = (data, callback) => {

    // need to verify all user data is sent:
    //  We should store their name, email address, and street address.

    // put required fields in an object, pass through validation, then create user
    var payload = {
        firstName   : data.payload.firstName,
        lastName    : data.payload.lastName,
        email       : data.payload.email,
        phoneNumber : data.payload.phoneNumber,
        zip         : data.payload.zip,
        address     : data.payload.address,
        city        : data.payload.city,
        tos         : data.payload.tos,
        password    : data.payload.password,
        confirmPassword : data.payload.confirmPassword
    };

    // validations
    val.check(payload, function (err, results) {
        if(err){
            callback(400, results)
        } else {
            // TODO: Passes validations: user look up to make sure user does not have an account with the same email
            _data.list('users', function (err, userList) {
               if(!err && userList && userList.length > 0){
                   if(userList.indexOf(payload.email) > -1){
                       callback(400, {"Error" : "A user with the same email already exist. Please try logging in or use a different email address."})
                   } else {
                       // hash passowrd then remove password and confirmPassword
                       var hashedPassword = helpers.hash(payload.password);
                       payload.hashedPassword = hashedPassword;
                       delete payload.password;
                       delete payload.confirmPassword;

                       // Create user in user dir
                       _data.create('users', payload.email, payload, function (err) {
                           if(err){
                               callback(500, {"Error" : "There was an internal issue creating the account. Please try again."})
                           } else {
                               delete payload.hashedPassword;
                               callback(200,payload)
                           }
                       })
                   }
               } else {
                   callback(500, {"Error" : "Internal Error, cannot find users. Please try again."})
               }
            });
        }
    });
};





























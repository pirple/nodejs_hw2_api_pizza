/*
* Create and export configuration variables
* */

// Container for all the environments
var enviroments = {};


// Staging (default) env
enviroments.staging = {
    'httpPort' : 3000,
    'httpsPort': 3001,
    'envName' : 'staging',
    'hashingSecret': 'pizzaApiAppSecret',
    'maxChecks' : 5,
    'twilio' : {
        'accountSid' : 'ACb32d411ad7fe886aac54c665d25e5c5d',
        'authToken' : '9455e3eb3109edc12e3d8c92768f7a67',
        'fromPhone' : '15005550006'
    },
    'stripe' : {
        'key' : '',
        'secretKey' : ''
    },
    'mailgun' : {
        'key'   : '',
        'email' : ''
    }
};

// Prod
enviroments.production = {
    'httpPort' : 5000,
    'httpsPort' : 5001,
    'envName' : 'production',
    'hashingSecret': 'pizzaApiAppSecret',
    'maxChecks' : 5,
    'twilio' : {
        'accountSid' : '',
        'authToken' : '',
        'fromPhone' : ''
    }
};





// Determine which env was pased as a command-line argument
var currentEnvironment = typeof(process.env.NODE_ENV) == 'string' ? process.env.NODE_ENV.toLowerCase() : '';

var environmentToExport = typeof(enviroments[currentEnvironment]) == 'object' ? enviroments[currentEnvironment] :  enviroments.staging

module.exports = environmentToExport;
/*
*
*
*  Module console out with color
*  3 fields to pass through to
*  msg, is required, color and debugLevel is optional
*/



/*
*  Run debug Levels:
*  NODE_Debug=dev
*  NODE_Debug=trace
*  NODE_Debug=prodLogs
*
*  or to run multiple logs
*  NODE_Debug=dev,trace,prod
*
* */


// Empty color field will default to white
// Three levels of consoles,
// debugLevel = dev, trace, prod
// empty field defaults to regular console.log output



// Dependencies
const util = require('util');
const dev = util.debuglog('dev');
const trace = util.debuglog('trace');
const prod = util.debuglog('prod');


// Instantiate
var lib = {};



// log to go out
lib.log = (msg, color, debugLevel) => {
    // Checking message length is greater than 0
    msg = msg && msg.length > 0 ? msg : ' ';

    // Get color if available :
    var colorCode;
    if (color){
        switch (color) {
            case 'white' :
                colorCode = '\x1b[30m%s\x1b[0m';
                break;
            case 'red':
                colorCode = '\x1b[31m%s\x1b[0m';
                break;
            case 'green':
                colorCode = '\x1b[32m%s\x1b[0m';
                break;
            case 'yellow':
                colorCode = '\x1b[33m%s\x1b[0m';
                break;
            case 'blue':
                colorCode = '\x1b[34m%s\x1b[0m';
                break;
            case 'purple':
                colorCode = '\x1b[35m%s\x1b[0m';
                break;
            case 'cyan':
                colorCode = '\x1b[36m%s\x1b[0m';
                break;
            case 'gray':
                colorCode = '\x1b[37m%s\x1b[0m';
                break;
            default:
                colorCode = undefined;
                break;
        }
    }

    // Making sure debugLevel is a string
    debugLevel =  debugLevel && typeof debugLevel == 'string' ? debugLevel : 'x';
    if (debugLevel && ['dev', 'trace','prodLogs'].indexOf(debugLevel) > -1) {
        switch (debugLevel) {
            case 'dev':
                dev(colorCode, msg);
                break;
            case 'trace':
                trace(colorCode, msg);
                break;
            case 'prodLogs':
                prodLogs(colorCode, msg);
                break;
        }
    } else {
        if(colorCode == undefined || colorCode == '') {
            console.log(msg);
        } else {
            console.log(colorCode, msg);
        }
    }
};



































module.exports = lib;


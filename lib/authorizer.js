/*
*
* Authorizer will check the token and check for a valid session
* if token : no-session it will have to go through a indexOf > -1 for selected endpoints:
*
*
* */

// Dependencies
const _data = require('./data');
const c = require('./consoleLog');

// container
var lib = {};


lib.session = (data, callback) => {

    var token = data.headers && data.headers.token && typeof data.headers.token == 'string' ? data.headers.token : false;
    var pass = ['post'].indexOf(data.method) > -1 && ['auth'].indexOf(data.trimmedPath) > -1;
    var pass2 = ['post'].indexOf(data.method) > -1 && ['users'].indexOf(data.trimmedPath) > -1;


    if (token === 'no-session'){
        if(pass || pass2){
            callback(false) // err = false
        } else {
            callback(true) // there is an error
        }
    } else {
        // search for session, check for expiration date, if alive increment hit, update expires date, callback true
        _data.read('sessions', token, function (err, tokenData) {
            if(err){
                callback(true) // there is an error
            } else {
                // expiration date
                var expirationCheck = new Date().toISOString();
                var check = expirationCheck < tokenData.expires && data.queryStringObject.email == tokenData.email;
                if(!check){
                    callback(true) // expired token
                } else {
                   // increment hit and update expire time
                    var newDate = Date.now() + 1000 * 60 * 30
                    newDate = new Date(newDate).toISOString();
                    tokenData.expires = newDate;
                    tokenData.hits++;
                    _data.update('sessions', token, tokenData, function (err) {
                        if(err) {
                            c.log(`Error: could not update session hits and expire data: ${err}`, 'red');
                            callback(false); // letting the call go through with this error, This only occurs when it can't update session
                        } else {
                            callback(false);
                        }
                    });
                }
            }
        });
    }
};

























module.exports = lib;




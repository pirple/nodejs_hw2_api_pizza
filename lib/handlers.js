// Request Handlers:


// Dependencies
const c = require('./consoleLog');
const menu = require('../api/menu/get');
const authorize = require('./authorizer');
const querystring = require('querystring');
const config = require('./config');
const https = require('https');


// Define handlers:
var handlers = {};

handlers.sample = function(data, callback){
    // callback a http status code, and a payload object
    console.log(`4422 data: ${JSON.stringify(data, null, 3)}`);
    callback(406, {'name' : 'sample handler'})
};

// Not found handler
handlers.notFound = function (data, callback) {
    callback(404, {"Error" : "Invalid request"})
};





/*
* AUTH
*
* */
handlers.auth = (data, callback) => {
    var acceptableMethods = ['post', 'delete'];
    if(acceptableMethods.indexOf(data.method) > -1){
        handlers._auth[data.method](data, callback);
    } else {
        callback(405, {"Error" : "Method not accepted for this call."});
    }
};

// Container
handlers._auth = {};

// NOTES: POST /AUTH
// Create token
handlers._auth.post = (data, callback) => {
    authorize.session(data, function (err) {
        if(err){
            callback(400, {"Error" : "Unauthorized."})
        } else {
            const authPost = require('../api/auth/post');
            authPost.handler(data, function (statusCode, tokenData) { callback(statusCode, tokenData) })
        }
    });
};

// NOTES: DELETE /AUTH
handlers._auth.delete = (data, callback) => {
    authorize.session(data, function (err) {
        if(err){
            callback(400, {"Error" : "Unauthorized."})
        } else {
            const authDelete = require('../api/auth/delete');
            authDelete.handler(data, function (statusCode, data) { callback(statusCode, data) })
        }
    });
};


/*
* AUTH
*
* */






/*
* MENU
*
* */
// /menu - has only a get
handlers.menu = (data, callback) => {
    var acceptableMethods = ['get'];
    if(acceptableMethods.indexOf(data.method) > -1){
        handlers._menu[data.method](data, callback);
    } else {
        callback(405, {"Error" : "Method not accepted for this call."});
    }
};

handlers._menu = {};

// NOTES: GET /MENU
// TODO: add auth check first then call menu.handler
handlers._menu.get = (data, callback) => {
    authorize.session(data, function (err) {
        if(err){
            callback(400, {"Error" : "Unauthorized."})
        } else {
            menu.handler(data, function (statusCode, data) { callback(statusCode, data) });
        }
    });
};

/*
* MENU
*
* */






/*
* USER
*
* */
// User - post, get, put, delete
handlers.users = (data, callback) => {
    var acceptableMethods = ['get', 'post', 'put', 'delete'];
    if(acceptableMethods.indexOf(data.method) > -1){
        handlers._users[data.method](data, callback);
    } else {
        callback(405, {"Error" : "Method not accepted for this call."});
    }
};


// user container
handlers._users = {};

// NOTES: POST /USER
handlers._users.post = (data, callback) => {
    authorize.session(data, function (err) {
        if(err){
            callback(400, {"Error" : "Unauthorized."})
        } else {
            const userPost = require('../api/users/post');
            userPost.handler(data, function (statusCode, userData) { callback(statusCode, userData) });
        }
    });
};

// NOTES: GET /USER
handlers._users.get = (data, callback) => {
    authorize.session(data, function (err) {
        if(err){
            callback(400, {"Error" : "Unauthorized."})
        } else {
            const userGet = require('../api/users/get');
            userGet.handler(data, function (statusCode, userData) { callback(statusCode, userData) });
        }
    });
};

// NOTES: PUT /USER
handlers._users.put = (data, callback) => {
    authorize.session(data, function (err) {
        if(err){
            callback(400, {"Error" : "Unauthorized."})
        } else {
            const userPut = require('../api/users/put');
            userPut.handler(data, function (statusCode, userData) { callback(statusCode, userData) });
        }
    });
};

// NOTES: DELETE /USER
handlers._users.delete = (data,callback) => {
    authorize.session(data, function (err) {
        if(err){
            callback(400, {"Error" : "Unauthorized."})
        } else {
            const userDelete = require('../api/users/delete');
            userDelete.handler(data, function (statusCode, results) { callback(statusCode, results) });
        }
    });
};


/*
* USER
*
* */






/*
* Add to cart
*
* */
// User - post, , ,
handlers.cart = (data, callback) => {
    var acceptableMethods = ['post'];
    if(acceptableMethods.indexOf(data.method) > -1){
        handlers._cart[data.method](data, callback);
    } else {
        callback(405, {"Error" : "Method not accepted for this call."});
    }
};

// Container:
handlers._cart = {};


// Add to shopping cart POST:
handlers._cart.post = (data, callback) => {
    authorize.session(data, function (err) {
        if(err){
            callback(400, {"Error" : "Unauthorized."})
        } else {
            const addToCart = require('../api/shoppingCart/post');
            addToCart.handler(data, function (statusCode, results) { callback(statusCode, results) });
        }
    });
};



// // User - post, get, put, delete
// handlers.order = (data, callback) => {
//     var acceptableMethods = ['post', 'get', 'delete'];
//     if(acceptableMethods.indexOf(data.method) > -1){
//         handlers._order[data.method](data, callback);
//     } else {
//         callback(405, {"Error" : "Method not accepted for this call."});
//     }
// };
//
// handlers._order = {};
//


// order
// Requires payment method send email

handlers.ordered = (data, callback) => {
    console.log(`4422 data: ${JSON.stringify(data, null, 3)}`);
    var acceptableMethods = ['get', 'post', 'delete'];
    if(acceptableMethods.indexOf(data.method) > -1){
        handlers._order[data.method](data, callback);
    } else {
        callback(405, {"Error" : "Method not accepted for this call."});
    }
};

handlers._order = {};

handlers._order.post = (data, callback) => {
    authorize.session(data, function (err) {
        if(err){
            callback(400, {"Error" : "Unauthorized."})
        } else {
            const ordered = require('../api/order/post');
            ordered.handler(data, function (statusCode, userData) { callback(statusCode, userData) });
        }
    });
};



// get cart
handlers._order.get = (data, callback) => {
    authorize.session(data, function (err) {
        if(err){
            callback(400, {"Error" : "Unauthorized."})
        } else {
            const getOrder = require('../api/order/get');
            getOrder.handler(data, function (statusCode, results) { callback(statusCode, results) });
        }
    });
};

// delete cart
handlers._order.delete = (data, callback) => {
    authorize.session(data, function (err) {
        if(err){
            callback(400, {"Error" : "Unauthorized."})
        } else {
            const deleteOrder = require('../api/order/delete');
            deleteOrder.handler(data, function (statusCode, results) { callback(statusCode, results) });
        }
    });
};



// Authorize check should be called before calling handlers in the api folder: ------>
// Export the module
module.exports = handlers;






























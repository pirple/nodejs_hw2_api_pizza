/*
*
*  Start server
*  Route to hanlder.js
*
* */



//Sever-related task:

// Dependencies
const http = require('http');
const https = require('https');
const url = require('url');
const StringDecoder = require('string_decoder').StringDecoder;
const config = require('./config');
const fs = require('fs');
const handlers = require('./handlers');
const helpers = require('./helpers');
const path = require('path');
const util = require('util');
const debug = util.debuglog('server');


// Instantiate the server module object
var server = {};



// Instantiate the HTTP Server
server.httpServer = http.createServer(function(req, res){
    server.unifiedServer(req,res);
});



// Instantiate the HTTPS Server
server.httpsServerOptions = {
    'key' : fs.readFileSync(path.join(__dirname, '/../https/key.pem')),
    'cert':fs.readFileSync(path.join(__dirname, '/../https/cert.pem'))
};

server.httpsServer = https.createServer(server.httpsServerOptions,function(req, res){
    unifiedServer(req,res);
});



// all the server logic for both the http and https server
server.unifiedServer = function (req, res) {


    // Get the URL and parse it
    let parsedUrl = url.parse(req.url,true);

    // Get the path
    let path = parsedUrl.pathname;
    let trimmedPath = path.replace(/^\/+|\/+$/g, '');

    // get the query sting as an object
    let queryStringObject = parsedUrl.query;


    // get http method
    let method = req.method.toLowerCase();

    // get the headers as an object
    let headers = req.headers;

    // get the payload if any
    let decoder = new StringDecoder('utf-8');
    var buffer = '';
    req.on('data', function (data) {
        buffer += decoder.write(data);
    });

    req.on('end',function () {
        buffer += decoder.end();

        // choose the handler this request should go to if one is not found use notfound hanlder
        let chosenHandler = typeof(server.router)[trimmedPath] !== 'undefined' ? server.router[trimmedPath] : handlers.notFound;

        //construct the data object to send to the handler
        let data = {
            'trimmedPath' : trimmedPath,
            'queryStringObject' : queryStringObject,
            'method' : method,
            'headers' : headers,
            'payload' : helpers.parseJsonToObject(buffer)
        };
        
        chosenHandler(data, function (statusCode, payload) {
            statusCode = typeof(statusCode) == 'number' ? statusCode : 200;

            payload = typeof payload == 'object' ? payload : {};

            // convert to string
            var payloadString = JSON.stringify(payload);

            res.setHeader('Content-Type', 'application/json');
            res.writeHead(statusCode);
            res.end(payloadString);

            //If the response is 200, print green otherwise print red
            if(statusCode == 200){
                debug('\x1b[32m%s\x1b[0m', `${method.toUpperCase()}/${trimmedPath} ${statusCode}`);
            } else {
                debug('\x1b[31m%s\x1b[0m', `${method.toUpperCase()}/${trimmedPath} ${statusCode}`);
            }
        });
    });

};


// Define a request router
server.router = {
    'menu'         : handlers.menu,
    'users'        : handlers.users,
    'auth'         : handlers.auth,
    'cart'         : handlers.cart,
    'order'        : handlers.ordered
};



// Init script
server.init = function(){
    // Start HTTP server
    server.httpServer.listen(config.httpPort, function(){
        console.log('\x1b[35m%s\x1b[0m',`The server is listening on port ${config.httpPort}`);

    });

    // Start HTTPS server
    server.httpsServer.listen(config.httpsPort, function(){
        console.log('\x1b[36m%s\x1b[0m',`The server is listening on port ${config.httpsPort}`);

    });

};



// Export the module
module.exports = server;













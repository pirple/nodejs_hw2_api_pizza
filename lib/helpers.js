// helpers for various tasks:



// Dependencies:
const crypto = require('crypto');
const config = require('./config');
const https = require('https');
const querystring = require('querystring');


// Container for all the Helpers
var helpers = {};




// Create a SHA256 hash

helpers.hash = function(str){

    if(typeof str == 'string' && str.length > 0){
        var hash = crypto.createHmac('sha256', config.hashingSecret).update(str).digest('hex');
        return hash;
    } else {
        return false
    }

};



//  Parse a JSON String to an objec tin all cases, without throwing:
helpers.parseJsonToObject = function(str){
    try {
        var obj = JSON.parse(str);
        return obj;
    } catch(err) {
        return {}
    }
};



// Create a string of random alphanumeric characters, of a given length
helpers.createRandomString = function (strLength) {
    strLength = typeof strLength == 'number' && strLength > 0 ? strLength : false;

    if(strLength){
        // defined possible characters:

        var possibleCharacters = 'abcdefghijklmnopqrstuvwxyz1234567890';

        var str = '';

        for (i = 1; i <= strLength; i++) {
            // append characters:
            var randomCharacter  = possibleCharacters.charAt(Math.floor(Math.random() * possibleCharacters.length));
            str += randomCharacter;
        }

        return str;

    } else {
        return false
    }

};






// Send an SMS message via twilio
helpers.sendTwilioSms = function(phone, msg, callback) {

    phone = typeof phone == 'string' && phone.trim().length == 10 ? phone.trim() : false;
    msg = typeof msg == 'string' && msg.trim().length > 0 && msg.trim().length <= 1600 ? msg.trim() : false;

    if(phone && msg) {
        // config request payload:
        var payload = {
            "From" : config.twilio.fromPhone,
            'To' : '+1' + phone,
            'Body' : msg
        };

        // stringify the payload:
        var stringPayload = querystring.stringify(payload);

        // configure request details
        var requestDetails = {
            'protocol' : 'https:',
            'hostname' : 'api.twilio.com',
            'method'   : "Post",
            'path'     : `/2010-04-01/Accounts/${config.twilio.accountSid}/Messages.json`,
            'auth'     : config.twilio.accountSid + ':' + config.twilio.authToken,
            'headers'  : {
                'Content-Type'   : 'application/x-www-form-urlencoded',
                'Content-Length' : Buffer.byteLength(stringPayload)
            }
        };


        // Instantiate the request object
        var req = https.request(requestDetails, function (res) {
            // Grab the status of the sent request
            var status = res.statusCode;
            // callback successfully
            if(status == 200 || status == 201)  {
                console.log(`4422 res: ${res}`);
                callback(false)
            } else {
                callback(`Status code returned was ${status} with : ${res}`)
            }
        });

        // Bind to the error event so it doesn't get thrown:
        req.on('error', function (e) {
            console.log(`4422  there is an error: ${JSON.stringify(e, null, 3)}`);
            callback(e)
        });

        // add the payload
        req.write(stringPayload);

        // end request
        req.end();


    } else {
        callback('Given parameters were missing or invalid.')
    }


};


helpers.sendEmail = (messageText, messageHtml, subject, email, callback) => {

    if ( (!messageText && !messageHtml) || !email || !subject) {
        callback(400, {"Error" : 'Please pass through all missing fields'});
    } else {

        // TODO: using config email to send to myself. use email from what is being passed through to send to the correct user email.

        let payload = {
            from    : `Vegan-Pizza-App ${config.mailgun.email}`,
            to      : `email ${config.mailgun.email}`,
            subject : subject,
            text    : messageText ? messageText : '',
            html    : messageHtml ? messageHtml : ''

        };

        let stringPayload = querystring.stringify(payload);


        // request option
        let options = {
            host: 'api.mailgun.net',
            port: 443,
            method: 'POST',
            path: '/v3/sandbox2da7762829cc4c3f9f699fb729f28caf.mailgun.org/messages',
            auth: 'api:'+config.mailgun.key,
            headers: {
                'Content-Type'   : 'application/x-www-form-urlencoded',
                'Content-Length' : Buffer.byteLength(stringPayload),
            }
        };

        // request object
        let req = https.request(options, function (res) {
            let result = '';
            res.on('data', function (chunk) {
                result += chunk;
            });
            res.on('end', function () {
                console.log(result);
                callback(200, result);
            });
            res.on('error', function (err) {
                console.log(err);
                callback(400, {"Error" : "There was an error sending your email receipt"});
            })
        });

        // req error
        req.on('error', function (err) {
            console.log(err);
            callback(400, {"Error" : "There was an error sending your email receipt"});
        });

        //send request witht the postData form
        req.write(stringPayload);
        req.end();
    }
};


// If i want to save payment method and tokens need to crete a new collection to hold payment info and move this function in another file
//payment is an object and must have payment.amount, everything else will default if not passed through.
helpers.payment = (payment, callback) => {

    if (!payment.amount){
        callback(400, {"Error" : "Missing payment.amount."})
    } else {
        // can do decline payments later if needed:
        let cardToken = payment.cardToken ? payment.cardToken : 'tok_visa';

        let payload = {
            amount: parseInt(payment.amount),
            currency: 'usd',
            description: payment.description ? payment.description : 'Pizza payment',
            source: cardToken
        };

        let stringPayload = querystring.stringify(payload);

        // request option
        let options = {
            host: 'api.stripe.com',
            port: 443,
            method: 'POST',
            path: '/v1/charges',
            headers: {
                'Content-Type'   : 'application/x-www-form-urlencoded',
                'Content-Length' : Buffer.byteLength(stringPayload),
                'Authorization'  : 'Bearer ' + config.stripe.secretKey
            }
        };

        // request object
        let req = https.request(options, function (res) {
            let result = '';
            res.on('data', function (chunk) {
                result += chunk;
            });
            res.on('end', function () {
                callback(result);
            });
            res.on('error', function (err) {
                console.log(err);
            })
        });

        // req error
        req.on('error', function (err) {
            console.log(err);
        });

        //send request witht the postData form
        req.write(stringPayload);
        req.end();
    }
};













// Export the module
module.exports = helpers;



/*
*
*
*
*
*
*
* */


// Dependencies
const menu = require('../.data/menu/menu');
const helpers = require('./helpers');


var lib = {};


lib.getOrderCost = (orders, callback) => {
        let orderReceipt = [{"totalCost": 0.00}];


        // calling last order for callback
        orders.push({"order-type" : "done"});

        orders.forEach((order) => {
            switch (order['order-type']) {
                case 'pizza':
                    lib.pizza(order, function (err, results) {
                        if(err) {
                           console.log('\x1b[33m%s\x1b[0m',`Something went wrong with getting the price: ${JSON.stringify(results)}`);
                           callback(400, results)
                        } else {
                            orderReceipt[0].totalCost += results.cost;
                            orderReceipt.push(results)
                        }
                    });
                    break;
                case "side":
                    lib.side(order, function (err, results) {
                        if(err) {
                            console.log('\x1b[33m%s\x1b[0m',`Something went wrong with getting the price: ${JSON.stringify(results)}`);
                            callback(400, results)
                        } else {
                            orderReceipt[0].totalCost += results.cost;
                            orderReceipt.push(results)
                        }
                    });
                    break;
                case "dessert":
                    lib.dessert(order, function (err, results) {
                        if(err) {
                            console.log('\x1b[33m%s\x1b[0m',`Something went wrong with getting the price: ${JSON.stringify(results)}`);
                            callback(400, results)
                        } else {
                            orderReceipt[0].totalCost += results.cost;
                            orderReceipt.push(results)
                        }
                    });
                    break;
                case "drink":
                    lib.drink(order, function (err, results) {
                        if(err) {
                            console.log('\x1b[33m%s\x1b[0m',`Something went wrong with getting the price: ${JSON.stringify(results)}`);
                            callback(400, results)
                        } else {
                            orderReceipt[0].totalCost += results.cost;
                            orderReceipt.push(results)
                        }
                    });
                    break;
                case "extra":
                    lib.extra(order, function (err, results) {
                        if(err) {
                            console.log('\x1b[33m%s\x1b[0m',`Something went wrong with getting the price: ${JSON.stringify(results)}`);
                            callback(400, results)
                        } else {
                            orderReceipt[0].totalCost += results.cost;
                            orderReceipt.push(results)
                        }
                    });
                    break;
                case "done":
                    var orderNumber = helpers.createRandomString(25);
                    orderReceipt[0].orderNumber = orderNumber;
                    orderReceipt[0].totalCost = orderReceipt[0].totalCost.toFixed(2);
                    callback(false, orderReceipt);
                    break;
                default:
                    break;
            }
        });
};








lib.pizza = (order, callback) => {
    // run validation first:
    //  console.log('\x1b[33m%s\x1b[0m',`lib.pizza --------->`);
     

    let orderType = order['order-type'] && order['order-type'] == 'pizza' ? order['order-type'] : false;
    let orderAmount = order['order-amount'] && typeof order['order-amount'] == 'number' && order['order-amount'] > 0 ? order['order-amount'] : false;
    let pizzaSize = order['pizza-size'] && ['small', 'medium', 'large', 'extra-large'].indexOf(order['pizza-size']) > -1 ? order['pizza-size'] : false;
    let crustType = order['crust-type'] && ['original', 'thin', 'pan', 'gluten-free', 'handtoss'].indexOf(order['crust-type']) > -1 ? order['crust-type'] : false;
    let cut = order['cut'] && ['normal', 'square'].indexOf(order['cut']) > -1 ? order['cut'] : false;
    let bake = order['bake'] && ['normal', 'well-done'].indexOf(order['bake']) > -1 ? order['bake'] : false;
    let sauce = order['sauce'] && ['original', 'buffalo', 'bbq', 'alfredo-sauce'].indexOf(order['sauce']) > -1 ? order['sauce'] : false;
    let sauceAmount = order['sauce-amount'] && ['normal', 'light', 'extra', 'none'].indexOf(order['sauce-amount']) > -1 ? order['sauce-amount'] : false;
    let cheeseAmount = order['cheese-amount'] && ['normal', 'light', 'extra', 'none'].indexOf(order['cheese-amount']) > -1 ? order['cheese-amount'] : false;
    let veganProtein = order['vegan-protein'] && Array.isArray(order['vegan-protein']) ? order['vegan-protein'] : [];
    let toppings = order['toppings'] && Array.isArray(order['toppings']) ? order['toppings'] : [];


    // check each one, all is require
    if (!orderType) {
        callback(true, {"Error": `Invalid order-type on one or more orders ${orderType} order-type.`})
    } else if (!orderAmount) {
        callback(true, {"Error": `Invalid order-amount on one or more orders ${orderType} order-type.`})
    } else if (!pizzaSize) {
        callback(true, {"Error": `Invalid pizza-size on one or more orders ${orderType} order-type.`})
    } else if (!crustType) {
        callback(true, {"Error": `Invalid crust-type on one or more orders ${orderType} order-type.`})
    } else if (!cut) {
        callback(true, {"Error": `Invalid cut on one or more orders ${orderType} order-type.`})
    } else if (!bake) {
        callback(true, {"Error": `Invalid bake on one or more orders ${orderType} order-type.`})
    } else if (!sauce) {
        callback(true, {"Error": `Invalid sauce on one or more orders ${orderType} order-type.`})
    } else if (!sauceAmount) {
        callback(true, {"Error": `Invalid sauce-amount on one or more orders ${orderType} order-type.`})
    } else if (!cheeseAmount) {
        callback(true, {"Error": `Invalid cheese-amount on one or more orders ${orderType} order-type.`})
    } else {

         var toppingsCost = 0.00;
         var veganProteinCost = 0.00;
         lib._pizza.toppings(toppings, function (err, tCost) {

             if(err) {
                 callback(400, {"Error" : "Invalid toppings on one or more orders"})
             } else {
                 toppingsCost = tCost;
                 lib._pizza.vegP(veganProtein, function (err, vCost) {
                     if(err){
                         callback(400, {"Error" : `Invalid vegan-protein on one or more orders ${orderType} order-type.`});
                     } else {
                         veganProteinCost = vCost;

                         // all validations are complete. Get the total cost for this order
                         let pizzaSizeCost = menu.pizza['pizza-sizes'][pizzaSize].price;

                         let cheeseAmountCost = menu.toppings['vegan-cheese-amount'][cheeseAmount].price;

                         let orderDetails = {
                             "cost" : ( pizzaSizeCost + cheeseAmountCost + veganProteinCost + toppingsCost ) * orderAmount,
                             "pizza-size" : pizzaSizeCost,
                             "order-amount" : orderAmount,
                             "cheese"     : cheeseAmountCost,
                             "vegan-protein" : veganProteinCost,
                             "toppings"      : toppingsCost,
                             "order-details" : {
                                 "order-amount" : orderAmount,
                                 'pizza-size'   : pizzaSize,
                                 'crust-type'   : crustType,
                                 "cut"          : cut,
                                 "bake"         : bake,
                                 'sauce'        : sauce,
                                 'sauce-amount' : sauceAmount,
                                 'vegan-cheese-amount' : cheeseAmount,
                                 'vegan-protein'       : veganProtein,
                                 'toppings'            : toppings
                             }
                         };
                         callback(false, orderDetails)
                     }
                 });
             }
         });
    }
};

lib._pizza = {};
lib._pizza.toppings = (toppings, callback) => {
    var toppingCheck = false;

    if (toppings.length > 0) {
        let q = 1;
        toppings.forEach((top) => {
            let i = ["mushrooms", "tomatoes", "pineapple", "onions", "black olives", "spinach", "jalapenos", "banana peppers", "green peppers"].indexOf(top) > -1 ? true : false;
            if (!i) {
                toppingCheck = true;
            }
            if(toppings.length == q ){
                cost = toppings.length > 2 ? (toppings.length -2) * 1.25 : 0.00;
                callback(toppingCheck, cost)
            }
            q++;
        })
    }
};


lib._pizza.vegP = (protein, callback) => {
    var veganProteinCheck = false;
    let w = 1;
    if (protein.length > 0) {
        var veganProteinCost = 0.00;
        protein.forEach((vegP) => {
            let k = ['tofu', 'tempeh', 'jackfruit'].indexOf(vegP) > -1 ? true : false;
            if (!k) {
                veganProteinCheck = true;
            } else {
                veganProteinCost += menu.toppings['vegan-protein'][vegP].price;
            }
            if(protein.length == w) {
                callback(veganProteinCheck, veganProteinCost)
             }
            w++;
        })
    }
};




lib.side = (order, callback) => {

    // validate the side required fields:
    let orderType = order['order-type'] && order['order-type'] == 'side' ? order['order-type'] : false;
    let orderAmount = order['order-amount'] && typeof order['order-amount'] == 'number' && order['order-amount'] > 0 ? order['order-amount'] : false;
    let sideType = order['side-type'] && typeof order['side-type'] == 'string' && ['garlic-knots', "garlic-breadsticks","original-breadsticks", "cauliflower-wings", "garden-salad" ].indexOf(order['side-type']) > -1 ? order['side-type'] : false;
    let pieces = order.pieces ? order.pieces : false;

    if(!orderType){
        callback(true, {"Error" : "Invalid order-type for one or more orders. Sides order-type"})
    }
    else if (!orderAmount) {
        callback(true, {"Error" : `Invalid order-type for one or more ${orderType} order-type`})
    }
    else if (!sideType) {
        callback(true, {"Error" : `Invalid side-type for one or more ${orderType} order-type`})
    } else if (!pieces) {
        callback(true, {"Error" : `Invalid pieces for one or more ${orderType} order-type`})
    } else {

        let sideCost = menu.sides[sideType].pieces[pieces].price;

        if(!sideCost){
            callback(true, {"Error" : `Cannot locate the price on one or more orders ${orderType} order-type`})
        }
        let orderDetails = {
            "cost" : sideCost * orderAmount,
            'order-details' : {
                'side-type' : sideType,
                'side-cost' : sideCost,
                'order-amount' : orderAmount,
                'pieces' : pieces
            }
        };
        callback(false, orderDetails)
    }
};

lib.dessert = (order, callback) => {

    // validate the side required fields:
    let orderType = order['order-type'] && order['order-type'] == 'dessert' ? order['order-type'] : false;
    let orderAmount = order['order-amount'] && typeof order['order-amount'] == 'number' && order['order-amount'] > 0 ? order['order-amount'] : false;
    let sideType = order['side-type'] && typeof order['side-type'] == 'string' && ['cinnamon-roll', "cookie","brownie"].indexOf(order['side-type']) > -1 ? order['side-type'] : false;
    let pieces = order.pieces ? order.pieces : false;

    if(!orderType){
        callback(true, {"Error" : "Invalid order-type for one or more orders. Sides order-type"})
    }
    else if (!orderAmount) {
        callback(true, {"Error" : `Invalid order-type for one or more ${orderType} order-type`})
    }
    else if (!sideType) {
        callback(true, {"Error" : `Invalid side-type for one or more ${orderType} order-type`})
    } else if (!pieces) {
        callback(true, {"Error" : `Invalid pieces for one or more ${orderType} order-type`})
    } else {

        let sideCost = menu.desserts[sideType].pieces[pieces].price;

        if(!sideCost){
            callback(true, {"Error" : `Cannot locate the price on one or more orders ${orderType} order-type`})
        }
        let orderDetails = {
            "cost" : sideCost * orderAmount,
            'order-details' : {
                'side-type' : sideType,
                'side-cost' : sideCost,
                'order-amount' : orderAmount,
                'pieces' : pieces
            }
        };
        callback(false, orderDetails)
    }
}; 



lib.drink = (order, callback) => {

    // validate the side required fields:
    let orderType = order['order-type'] && order['order-type'] == 'drink' ? order['order-type'] : false;
    let orderAmount = order['order-amount'] && typeof order['order-amount'] == 'number' && order['order-amount'] > 0 ? order['order-amount'] : false;
    let sideType = order['side-type'] && typeof order['side-type'] == 'string' && ['pepsi', "mountain-dew","canada-dry", "sierra-mist"].indexOf(order['side-type']) > -1 ? order['side-type'] : false;
    let size = order.size && ['2-liter', '20-oz'].indexOf(order.size) > -1? order.size : false;

    if(!orderType){
        callback(true, {"Error" : "Invalid order-type for one or more orders. Sides order-type"})
    }
    else if (!orderAmount) {
        callback(true, {"Error" : `Invalid order-type for one or more ${orderType} order-type`})
    }
    else if (!sideType) {
        callback(true, {"Error" : `Invalid side-type for one or more ${orderType} order-type`})
    } else if (!size) {
        callback(true, {"Error" : `Invalid pieces for one or more ${orderType} order-type`})
    } else {

        let sideCost = menu.drinks[sideType].size[size].price;

        if(!sideCost){
            callback(true, {"Error" : `Cannot locate the price on one or more orders ${orderType} order-type`})
        }
        let orderDetails = {
            "cost" : sideCost * orderAmount,
            'order-details' : {
                'side-type' : sideType,
                'side-cost' : sideCost,
                'order-amount' : orderAmount,
                'size' : size
            }
        };
        callback(false, orderDetails)
    }
};

lib.extra = (order, callback) => {

    // validate the side required fields:
    let orderType = order['order-type'] && order['order-type'] == 'extra' ? order['order-type'] : false;
    let orderAmount = order['order-amount'] && typeof order['order-amount'] == 'number' && order['order-amount'] > 0 ? order['order-amount'] : false;
    let sideType = order['side-type'] && typeof order['side-type'] == 'string' && ['garlic-sauce', "spicy-garlic-sauce","crushed-red-pepper", "pizza-sauce"].indexOf(order['side-type']) > -1 ? order['side-type'] : false;

    if(!orderType){
        callback(true, {"Error" : "Invalid order-type for one or more orders. Sides order-type"})
    }
    else if (!orderAmount) {
        callback(true, {"Error" : `Invalid order-type for one or more ${orderType} order-type`})
    }
    else if (!sideType) {
        callback(true, {"Error" : `Invalid side-type for one or more ${orderType} order-type`})
    }  else {

        let sideCost = menu.extras[sideType].price;

        if(!sideCost){
            callback(true, {"Error" : `Cannot locate the price on one or more orders ${orderType} order-type`})
        }
        let orderDetails = {
            "cost" : sideCost * orderAmount,
            'order-details' : {
                'side-type' : sideType,
                'side-cost' : sideCost,
                'order-amount' : orderAmount,
            }
        };
        callback(false, orderDetails)
    }
};

module.exports = lib;



